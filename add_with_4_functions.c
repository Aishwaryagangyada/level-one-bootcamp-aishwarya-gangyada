//Write a program to add two user input numbers using 4 functions.
#include <stdio.h>
int input()
{
   int h;
   printf("Enter a number\n");
   scanf("%d",&h);
   return h;
}
int sum(int h, int u)
{
   int sum;
   sum = h+u;
   return sum;
}
void output(int h, int u, int v)
{
   printf("Sum of %d and %d is %d\n",h,u,v);
}
int main()
{
   int x,y,z;
   x=input();
   y=input();
   z=sum(x,y);
   output(x,y,z);
   return 0;
}//Write a program to add two user input numbers using 4 functions.